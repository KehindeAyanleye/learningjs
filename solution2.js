// Write a program to calculate the total price of your phone purchase. You will keep purchasing phones (hint: loop!) until you run out of money in your bank account.

// You'll also buy accessories for each phone as long as your purchase amount is below your mental spending threshold.

// After you've calculated your purchase amount, add in the tax, then print out the calculated purchase amount, properly formatted.

// Finally, check the amount against your bank account balance to see if you can afford it or not.

// You should set up some constants for the "tax rate," "phone price," "accessory price," and "spending threshold," as well as a variable for your "bank account balance.""
// You should define functions for calculating the tax and for formatting the price with a "$" and rounding to two decimal places.
// Bonus Challenge: Try to incorporate input into this program, perhaps with the prompt(..) covered in "Input" earlier. You may prompt the user for their bank account balance, for example. Have fun and be creative!
// OK, go ahead. Try it. Don't peek at my code listing until you've given it a shot yourself!

// Note: Because this is a JavaScript book, I'm obviously going to solve the practice exercise in JavaScript. But you can do it in another language for now if you feel more comfortable.

const SPENDING_THRESHOLD = 450;
const TAX_RATE = 0.08;
const PHONE_PRICE = 195.98;
const ACCESSORY_PRICE = 35.89;

var bank_balance = 550;
var amount = 0;
var new_balance = 0;

// calculating the tax
function calculateTax(amount){
    return amount * TAX_RATE;
}

// formating amount
function formatAmount(amount){
    return "$" + amount.toFixed( 2 );
}

// formating percentage
function formatPercentage(new_balance){
    return new_balance.toFixed( 2 );
}

while(amount < SPENDING_THRESHOLD){ // 0, 231.87, 463.74
    // buy phone
    amount = amount + PHONE_PRICE; // 195.98 >> 427.85 >> 659.72

    // can I buy accessory
    if(amount < bank_balance){ // 195.98, 427.85, 659.72
        amount = amount + ACCESSORY_PRICE; // 231.87 >> 463.74
    }
}

// lets pay the government
amount = amount + calculateTax(amount);
new_balance = bank_balance - amount;

// let's print total purchase
if (amount < bank_balance) {
    console.log(
        "Yike, you have just purchased phones worth a total of " + formatAmount(amount)
    );

    // let's print initial balance
    console.log(
        "Your initial balance was " + formatAmount(bank_balance)
    );

    // let's print new balance
    console.log(
        "Your new balance is " + formatAmount(new_balance)
    );

    // let's print percentage spent
    console.log(
        "You have just spent " + formatPercentage(((bank_balance - new_balance) / bank_balance) * 100) + "% of your initial balance"
    );
} else {
    console.log("You can't afford this purchase of " + formatAmount(amount));
    
    // let's print initial balance
    console.log(
        "Your initial balance still remains " + formatAmount(bank_balance)
    );

    // let's print new balance
    console.log(
        "Your new balance after the purchase would have been " + formatAmount(new_balance)
    );

    // let's print percentage spent
    console.log(
        "If you had made that purchase you would have spent " + formatPercentage(((bank_balance - new_balance) / bank_balance) * 100) + "% of your initial balance"
    );
}