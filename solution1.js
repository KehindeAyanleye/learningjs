// Challenge 1
/*
There is absolutely no substitute for practice in learning programming. No amount of articulate writing on my part is alone going to make you a programmer.

With that in mind, let's try practicing some of the concepts we learned here in this chapter. I'll give the "requirements," and you try it first. Then consult the code listing below to see how I approached it.

Write a program to calculate the total price of your phone purchase. You will keep purchasing phones (hint: loop!) until you run out of money in your bank account. You'll also buy accessories for each phone as long as your purchase amount is below your mental spending threshold.
After you've calculated your purchase amount, add in the tax, then print out the calculated purchase amount, properly formatted.
Finally, check the amount against your bank account balance to see if you can afford it or not.
You should set up some constants for the "tax rate," "phone price," "accessory price," and "spending threshold," as well as a variable for your "bank account balance.""
You should define functions for calculating the tax and for formatting the price with a "$" and rounding to two decimal places.
Bonus Challenge: Try to incorporate input into this program, perhaps with the prompt(..) covered in "Input" earlier. You may prompt the user for their bank account balance, for example. Have fun and be creative!
OK, go ahead. Try it. Don't peek at my code listing until you've given it a shot yourself!

Note: Because this is a JavaScript book, I'm obviously going to solve the practice exercise in JavaScript. But you can do it in another language for now if you feel more comfortable.
*/

// Write a program to calculate the total price of your phone purchase. You will keep purchasing phones (hint: loop!) until you run out of money in your bank account.

var bank_balance = 900; // bank

for (var cost = 23; cost < bank_balance; cost += cost) {
    console.log(cost); // 23+46+92+184+368+736+1472
}

// You'll also buy accessories for each phone as long as your purchase amount is below your mental spending threshold.

const accessory_price = 9.22;

var bank_balance = 900; // bank

for (var cost = 23; cost < bank_balance; cost += cost) {
    cost += accessory_price;
    console.log(cost); // ((((((((23+9.22)*2)+9.22)*2)+9.22)*2)+9.22)*2)+9.22)
}

// After you've calculated your purchase amount, add in the tax, then print out the calculated purchase amount, properly formatted.
const tax = (5/100);

var bank_balance = 900; // bank

for (var cost = 23; cost < bank_balance; cost += cost) {
    cost += accessory_price;
    function printTotal(){
        console.log((cost + (cost * tax)/2).toFixed(0)); //686.511
    }
}