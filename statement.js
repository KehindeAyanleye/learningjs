// Function
// When you type the amount, you want to see the amount plus the tax = Total
// A: amount + tax = total


// When you type the amount, you want to see the tax only. = Tax
// B: amount * (100 / tax) = Total Tax

// Solution A
var tax = 5 / 100;
var amount = 350000;

function printTotal(){
    console.log((amount + (amount * tax)).toFixed(2));
}

printTotal(amount);

// Solution B
function printTotalTax(){
    console.log((amount * tax).toFixed(2));
}

printTotalTax(amount);


// Declare a function that repeats a couple of times in your console
    //  This will mean show a 5 times.

    var a = 5;
    var b = 89

    function emarun() {
        for (a = 5; a <= 10; a += 1) {
            console.log (a)
        }
    }

    emarun(b);

// 
var a = 10; // 10 

function foo(b) {
    a = a * 2; // (10 * 2 = 20) (25 * 2 = 50) (75 * 2 = 150)
    a = a + b; // (20 + 5 = 25) (50 + 25 = 75) (150 + 100 = 248)
}
foo(5); 
foo(25);
foo(100);
console.log(a); // 50


// what is this code
var a = 10; // 10 
var b = foo(3);

function foo(b) {
    a = a * 2; // (10 * 2 = 20)
    a = a + b; // (20 + 3 = 23)
    return a * 1; // b is 23 / 2 = 11.5

}

console.log(a); // 23
console.log(b); // 23

// interpret this code
var a = 10;
var b = foo(3);

function foo(b) {
    a = a * 2; // a is 10 *2 = 20
    a = a + b; // a is 20 + 3 = 23
    return a / 2; // b is 23 / 2 = 11.5
}

console.log(a); // 23
console.log(b); // 11.5

// Scope
// You can always access variables outside of your scope, but not inside.

function foo(b) {
    var c = 42;
    a = a * 2;
    a = a + b;
    return a / 2;
}

var a = 10;

console.log(b);